/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package noofoccurence;

import java.util.Scanner;

/**
 *
 * @author Usama
 */
public class NoOfOccurence {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int count = 0;
        String s; 
        Scanner in = new Scanner(System.in);
        System.out.println("Enter a string: ");
        s = in.nextLine();
        char m;
        System.out.println("Enter the character: ");
        m = in.nextLine().charAt(0);
        for(int i = 0; i<s.length(); i++){
            if(s.charAt(i)==m){
                count++;
            }
        }
        System.out.println("Total Occurences: " + count);
        
    }
    
}
