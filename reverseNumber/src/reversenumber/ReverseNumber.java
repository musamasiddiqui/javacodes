/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reversenumber;

/**
 *
 * @author Usama
 */
public class ReverseNumber {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int input=12345;
        int last_digit;
        int reversedNum=0;
        while (input!=0) {
            last_digit = input % 10;
            reversedNum = reversedNum * 10 + last_digit;
            input = input / 10;
            }
        System.out.println("reverse number is : "+reversedNum);
    }
    
}
