/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package portionstring;

/**
 *
 * @author Usama
 */
public class PortionString {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //Question 1 of Document 1
        
        String s = "Usama Siddiqui";
        
        //Extracting First Five Characters
        String n = s.substring(0, 5);
        
        System.out.println("Original String: " + s);
        
        System.out.println("Extracted String: " + n);
    }
    
}
