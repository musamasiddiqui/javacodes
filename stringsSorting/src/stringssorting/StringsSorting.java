/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stringssorting;
import java.util.Scanner;
/**
 *
 * @author Usama
 */
public class StringsSorting {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String temp;
        Scanner st = new Scanner(System.in);
        String array[] = new String[5];
        for(int i = 0; i < 5; i++){
            array[i] = st.nextLine();
        }
        System.out.println("\nStrings before sorting: ");
        for (String array1 : array) {
            System.out.println(array1);
        }
        System.out.println("\nStrings after sorting: ");
        
        for(int i = 0; i < 5; i++){
            for(int j = 1; j < 5 ; j++){
                if(array[j-1].compareTo(array[j]) > 0){
                    temp = array[j-1];
                    array[j-1] = array[j];
                    array[j] = temp;
                    
                }
            }
        }
        
        System.out.println("\nStrings after sorting: ");
        for (String array1 : array) {
            System.out.println(array1);
        }
    }
    
}
